require "application_system_test_case"

class ArticleTagRecordsTest < ApplicationSystemTestCase
  setup do
    @article_tag_record = article_tag_records(:one)
  end

  test "visiting the index" do
    visit article_tag_records_url
    assert_selector "h1", text: "Article Tag Records"
  end

  test "creating a Article tag record" do
    visit article_tag_records_url
    click_on "New Article Tag Record"

    fill_in "Article", with: @article_tag_record.article_id
    fill_in "Tag", with: @article_tag_record.tag_id
    click_on "Create Article tag record"

    assert_text "Article tag record was successfully created"
    click_on "Back"
  end

  test "updating a Article tag record" do
    visit article_tag_records_url
    click_on "Edit", match: :first

    fill_in "Article", with: @article_tag_record.article_id
    fill_in "Tag", with: @article_tag_record.tag_id
    click_on "Update Article tag record"

    assert_text "Article tag record was successfully updated"
    click_on "Back"
  end

  test "destroying a Article tag record" do
    visit article_tag_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Article tag record was successfully destroyed"
  end
end
