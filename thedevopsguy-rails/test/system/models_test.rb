require "application_system_test_case"

class ModelsTest < ApplicationSystemTestCase
  setup do
    @model = models(:one)
  end

  test "visiting the index" do
    visit models_url
    assert_selector "h1", text: "Models"
  end

  test "creating a Model" do
    visit models_url
    click_on "New Model"

    fill_in "Alias", with: @model.alias
    fill_in "Astrological", with: @model.astrological
    fill_in "Birthday", with: @model.birthday
    fill_in "City", with: @model.city
    fill_in "Country", with: @model.country
    fill_in "Debut-year", with: @model.debut-year
    fill_in "Eye-color", with: @model.eye-color
    fill_in "Hair-color", with: @model.hair-color
    fill_in "Height", with: @model.height
    fill_in "Measurement", with: @model.measurement
    fill_in "Piercings", with: @model.piercings
    fill_in "Tattoos", with: @model.tattoos
    click_on "Create Model"

    assert_text "Model was successfully created"
    click_on "Back"
  end

  test "updating a Model" do
    visit models_url
    click_on "Edit", match: :first

    fill_in "Alias", with: @model.alias
    fill_in "Astrological", with: @model.astrological
    fill_in "Birthday", with: @model.birthday
    fill_in "City", with: @model.city
    fill_in "Country", with: @model.country
    fill_in "Debut-year", with: @model.debut-year
    fill_in "Eye-color", with: @model.eye-color
    fill_in "Hair-color", with: @model.hair-color
    fill_in "Height", with: @model.height
    fill_in "Measurement", with: @model.measurement
    fill_in "Piercings", with: @model.piercings
    fill_in "Tattoos", with: @model.tattoos
    click_on "Update Model"

    assert_text "Model was successfully updated"
    click_on "Back"
  end

  test "destroying a Model" do
    visit models_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Model was successfully destroyed"
  end
end
