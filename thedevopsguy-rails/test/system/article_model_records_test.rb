require "application_system_test_case"

class ArticleModelRecordsTest < ApplicationSystemTestCase
  setup do
    @article_model_record = article_model_records(:one)
  end

  test "visiting the index" do
    visit article_model_records_url
    assert_selector "h1", text: "Article Model Records"
  end

  test "creating a Article model record" do
    visit article_model_records_url
    click_on "New Article Model Record"

    fill_in "Article", with: @article_model_record.article_id
    fill_in "Model", with: @article_model_record.model_id
    click_on "Create Article model record"

    assert_text "Article model record was successfully created"
    click_on "Back"
  end

  test "updating a Article model record" do
    visit article_model_records_url
    click_on "Edit", match: :first

    fill_in "Article", with: @article_model_record.article_id
    fill_in "Model", with: @article_model_record.model_id
    click_on "Update Article model record"

    assert_text "Article model record was successfully updated"
    click_on "Back"
  end

  test "destroying a Article model record" do
    visit article_model_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Article model record was successfully destroyed"
  end
end
