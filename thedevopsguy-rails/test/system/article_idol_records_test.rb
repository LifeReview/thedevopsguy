require "application_system_test_case"

class ArticleIdolRecordsTest < ApplicationSystemTestCase
  setup do
    @article_idol_record = article_idol_records(:one)
  end

  test "visiting the index" do
    visit article_idol_records_url
    assert_selector "h1", text: "Article Idol Records"
  end

  test "creating a Article idol record" do
    visit article_idol_records_url
    click_on "New Article Idol Record"

    fill_in "Article", with: @article_idol_record.article_id
    fill_in "Idol", with: @article_idol_record.idol_id
    click_on "Create Article idol record"

    assert_text "Article idol record was successfully created"
    click_on "Back"
  end

  test "updating a Article idol record" do
    visit article_idol_records_url
    click_on "Edit", match: :first

    fill_in "Article", with: @article_idol_record.article_id
    fill_in "Idol", with: @article_idol_record.idol_id
    click_on "Update Article idol record"

    assert_text "Article idol record was successfully updated"
    click_on "Back"
  end

  test "destroying a Article idol record" do
    visit article_idol_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Article idol record was successfully destroyed"
  end
end
