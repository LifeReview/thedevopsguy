require "application_system_test_case"

class IdolsTest < ApplicationSystemTestCase
  setup do
    @idol = idols(:one)
  end

  test "visiting the index" do
    visit idols_url
    assert_selector "h1", text: "Idols"
  end

  test "creating a Idol" do
    visit idols_url
    click_on "New Idol"

    fill_in "Alias", with: @idol.alias
    fill_in "Astrological", with: @idol.astrological
    fill_in "Birthday", with: @idol.birthday
    fill_in "City", with: @idol.city
    fill_in "Country", with: @idol.country
    fill_in "Debut-year", with: @idol.debut-year
    fill_in "Eye-color", with: @idol.eye-color
    fill_in "Hair-color", with: @idol.hair-color
    fill_in "Height", with: @idol.height
    fill_in "Measurement", with: @idol.measurement
    fill_in "Piercings", with: @idol.piercings
    fill_in "Tattoos", with: @idol.tattoos
    click_on "Create Idol"

    assert_text "Idol was successfully created"
    click_on "Back"
  end

  test "updating a Idol" do
    visit idols_url
    click_on "Edit", match: :first

    fill_in "Alias", with: @idol.alias
    fill_in "Astrological", with: @idol.astrological
    fill_in "Birthday", with: @idol.birthday
    fill_in "City", with: @idol.city
    fill_in "Country", with: @idol.country
    fill_in "Debut-year", with: @idol.debut-year
    fill_in "Eye-color", with: @idol.eye-color
    fill_in "Hair-color", with: @idol.hair-color
    fill_in "Height", with: @idol.height
    fill_in "Measurement", with: @idol.measurement
    fill_in "Piercings", with: @idol.piercings
    fill_in "Tattoos", with: @idol.tattoos
    click_on "Update Idol"

    assert_text "Idol was successfully updated"
    click_on "Back"
  end

  test "destroying a Idol" do
    visit idols_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Idol was successfully destroyed"
  end
end
