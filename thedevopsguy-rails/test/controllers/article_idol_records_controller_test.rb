require 'test_helper'

class ArticleIdolRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article_idol_record = article_idol_records(:one)
  end

  test "should get index" do
    get article_idol_records_url
    assert_response :success
  end

  test "should get new" do
    get new_article_idol_record_url
    assert_response :success
  end

  test "should create article_idol_record" do
    assert_difference('ArticleIdolRecord.count') do
      post article_idol_records_url, params: { article_idol_record: { article_id: @article_idol_record.article_id, idol_id: @article_idol_record.idol_id } }
    end

    assert_redirected_to article_idol_record_url(ArticleIdolRecord.last)
  end

  test "should show article_idol_record" do
    get article_idol_record_url(@article_idol_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_idol_record_url(@article_idol_record)
    assert_response :success
  end

  test "should update article_idol_record" do
    patch article_idol_record_url(@article_idol_record), params: { article_idol_record: { article_id: @article_idol_record.article_id, idol_id: @article_idol_record.idol_id } }
    assert_redirected_to article_idol_record_url(@article_idol_record)
  end

  test "should destroy article_idol_record" do
    assert_difference('ArticleIdolRecord.count', -1) do
      delete article_idol_record_url(@article_idol_record)
    end

    assert_redirected_to article_idol_records_url
  end
end
