require 'test_helper'

class ArticleTagRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article_tag_record = article_tag_records(:one)
  end

  test "should get index" do
    get article_tag_records_url
    assert_response :success
  end

  test "should get new" do
    get new_article_tag_record_url
    assert_response :success
  end

  test "should create article_tag_record" do
    assert_difference('ArticleTagRecord.count') do
      post article_tag_records_url, params: { article_tag_record: { article_id: @article_tag_record.article_id, tag_id: @article_tag_record.tag_id } }
    end

    assert_redirected_to article_tag_record_url(ArticleTagRecord.last)
  end

  test "should show article_tag_record" do
    get article_tag_record_url(@article_tag_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_tag_record_url(@article_tag_record)
    assert_response :success
  end

  test "should update article_tag_record" do
    patch article_tag_record_url(@article_tag_record), params: { article_tag_record: { article_id: @article_tag_record.article_id, tag_id: @article_tag_record.tag_id } }
    assert_redirected_to article_tag_record_url(@article_tag_record)
  end

  test "should destroy article_tag_record" do
    assert_difference('ArticleTagRecord.count', -1) do
      delete article_tag_record_url(@article_tag_record)
    end

    assert_redirected_to article_tag_records_url
  end
end
