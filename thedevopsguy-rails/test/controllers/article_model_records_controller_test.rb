require 'test_helper'

class ArticleModelRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @article_model_record = article_model_records(:one)
  end

  test "should get index" do
    get article_model_records_url
    assert_response :success
  end

  test "should get new" do
    get new_article_model_record_url
    assert_response :success
  end

  test "should create article_model_record" do
    assert_difference('ArticleModelRecord.count') do
      post article_model_records_url, params: { article_model_record: { article_id: @article_model_record.article_id, model_id: @article_model_record.model_id } }
    end

    assert_redirected_to article_model_record_url(ArticleModelRecord.last)
  end

  test "should show article_model_record" do
    get article_model_record_url(@article_model_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_article_model_record_url(@article_model_record)
    assert_response :success
  end

  test "should update article_model_record" do
    patch article_model_record_url(@article_model_record), params: { article_model_record: { article_id: @article_model_record.article_id, model_id: @article_model_record.model_id } }
    assert_redirected_to article_model_record_url(@article_model_record)
  end

  test "should destroy article_model_record" do
    assert_difference('ArticleModelRecord.count', -1) do
      delete article_model_record_url(@article_model_record)
    end

    assert_redirected_to article_model_records_url
  end
end
