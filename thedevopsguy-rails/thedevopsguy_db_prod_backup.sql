-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: thedevopsguy
-- ------------------------------------------------------
-- Server version	5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `active_admin_comments`
--

DROP TABLE IF EXISTS `active_admin_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `active_admin_comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `namespace` varchar(255) DEFAULT NULL,
  `body` text,
  `resource_type` varchar(255) DEFAULT NULL,
  `resource_id` bigint(20) DEFAULT NULL,
  `author_type` varchar(255) DEFAULT NULL,
  `author_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_active_admin_comments_on_resource_type_and_resource_id` (`resource_type`,`resource_id`),
  KEY `index_active_admin_comments_on_author_type_and_author_id` (`author_type`,`author_id`),
  KEY `index_active_admin_comments_on_namespace` (`namespace`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `active_admin_comments`
--

LOCK TABLES `active_admin_comments` WRITE;
/*!40000 ALTER TABLE `active_admin_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `active_admin_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_admin_users_on_email` (`email`),
  UNIQUE KEY `index_admin_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'tuan@gmail.com','$2a$11$mIWM9gS3by1MaWgCL8cFru57cACa8Zz1AoEZ0tmj2eLnqb23HXGX.',NULL,NULL,NULL,'2020-05-14 17:19:59','2020-05-18 17:46:54');
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ar_internal_metadata`
--

DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_internal_metadata`
--

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;
INSERT INTO `ar_internal_metadata` VALUES ('environment','production','2020-05-14 17:18:42','2020-05-14 17:18:42');
/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_idol_records`
--

DROP TABLE IF EXISTS `article_idol_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_idol_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) DEFAULT NULL,
  `idol_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_article_idol_records_on_article_id` (`article_id`),
  KEY `index_article_idol_records_on_idol_id` (`idol_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_idol_records`
--

LOCK TABLES `article_idol_records` WRITE;
/*!40000 ALTER TABLE `article_idol_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `article_idol_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_tag_records`
--

DROP TABLE IF EXISTS `article_tag_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article_tag_records` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) DEFAULT NULL,
  `tag_id` bigint(20) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_article_tag_records_on_article_id` (`article_id`),
  KEY `index_article_tag_records_on_tag_id` (`tag_id`),
  CONSTRAINT `fk_rails_78f17eb451` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `fk_rails_8b75949431` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_tag_records`
--

LOCK TABLES `article_tag_records` WRITE;
/*!40000 ALTER TABLE `article_tag_records` DISABLE KEYS */;
INSERT INTO `article_tag_records` VALUES (1,1,3,'2020-05-15 02:17:19','2020-05-15 02:17:19'),(2,1,2,'2020-05-15 04:35:57','2020-05-15 04:35:57'),(3,3,4,'2020-05-15 09:17:40','2020-05-15 09:17:40'),(4,3,1,'2020-05-15 09:17:40','2020-05-15 09:17:40'),(5,2,1,'2020-05-15 09:22:39','2020-05-15 09:22:39'),(6,5,2,'2020-05-18 17:51:57','2020-05-18 17:51:57'),(7,5,3,'2020-05-18 17:52:12','2020-05-18 17:52:12');
/*!40000 ALTER TABLE `article_tag_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `content` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `videolink` varchar(255) DEFAULT NULL,
  `visitcount` int(11) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'How to start using Rails Redcarpet and Coderay gems for markdown','This tutorial will lead you through the steps to start using Rails fancy gem Redcarpet for markdown','First, we need to include the gems\r\n\r\n```ruby\r\n#Gemfile\r\n\r\ngem \'redcarpet\', \'~> 2.1.1\'\r\ngem \'coderay\', \'~> 1.0.7\'\r\n```\r\n**Great, you have prepared the `red-carpet`**\r\n![alt_text](https://image-english.vov.vn/w500/uploaded/wzzqbijjvws/2018_05_10/Cannes_1_MUZZ.JPG)\r\n\r\nNext, run:\r\n\r\n```bash\r\n\r\nbundle install\r\n\r\n```\r\nNext, we will add a markdown function to application helper, which inherit from the gems, so that we can start using our markdown and coderay functions:\r\n \r\n```ruby\r\n#application_helper.rb\r\n\r\nmodule ApplicationHelper\r\nclass CodeRayify < Redcarpet::Render::HTML\r\n  def block_code(code, language)\r\n    CodeRay.scan(code, language).div\r\n  end\r\nend\r\n\r\ndef markdown(text)\r\n  coderayified = CodeRayify.new(:filter_html => true, \r\n                                :hard_wrap => true)\r\n  options = {\r\n    :fenced_code_blocks => true,\r\n    :no_intra_emphasis => true,\r\n    :autolink => true,\r\n    :lax_html_blocks => true,\r\n  }\r\n  markdown_to_html = Redcarpet::Markdown.new(coderayified, options)\r\n  markdown_to_html.render(text).html_safe\r\nend\r\nend\r\n```\r\nNext, in your view, you can specify the functions as this to start using markdown:\r\n\r\n```haml\r\n-# show.html.haml\r\n  .card-body          \r\n    %h5.card-title \r\n      =markdown(@article.name)\r\n    %p.card-text \r\n      =markdown(@article.description)\r\n    %p.card-text \r\n      =markdown(@article.content)\r\n```\r\n\r\nAfter that, we can go on and look at some instructions and rules for redcarpet and coderay at https://github.com/vmg/redcarpet and https://github.com/rubychan/coderay respectively.','2020-05-14 17:20:30','2020-06-24 04:54:53','https://image-english.vov.vn/w500/uploaded/wzzqbijjvws/2018_05_10/Cannes_1_MUZZ.JPG	',NULL,319,'how-to-start-using-rails-redcarpet-and-coderay-gems-for-markdown','approved'),(2,'How to display your Unix/Linux machine available diskspace for filesystem by `df -h` command','This tutorial will show you the meaning of the `df -h` command and how to use it','#### First, let look at what `df -h` is\r\n\r\nSo, basically `df ` is to displays the amount of disk space available on the filesystem containing each filename argument. If no filename is given, the space available on all currently mounted filesystems is shown.\r\n\r\nDisk space is shown in 1K blocks by default, unless the environment variable POSIXLY_CORRECT is set, in which case 512-byte blocks are used.\r\n\r\nIf an argument is the absolute filename of a disk device node containing a mounted filesystem, df shows the space available on that filesystem rather than on the filesystem containing the device node (which is always the root filesystem). This version of df cannot show the space available on unmounted filesystems, because on most kinds of systems doing so requires very nonportable, intimate knowledge of filesystem structures.\r\n\r\n### Next, how to use it in you Unix based machine ?\r\n\r\nIn your machine, type\r\n\r\n```bash\r\ndf \r\n\r\nFilesystem     1K-blocks     Used Available Use% Mounted on\r\nudev             1962312        0   1962312   0% /dev\r\ntmpfs             396976     1508    395468   1% /run\r\n/dev/nvme0n1p1  30428560 22893912   7518264  76% /\r\ntmpfs            1984872        0   1984872   0% /dev/shm\r\ntmpfs               5120        0      5120   0% /run/lock\r\ntmpfs            1984872        0   1984872   0% /sys/fs/cgroup\r\n/dev/loop0         96256    96256         0 100% /snap/core/9066\r\n/dev/loop1         96128    96128         0 100% /snap/core/8935\r\n/dev/loop2         18432    18432         0 100% /snap/amazon-ssm-agent/1455\r\n/dev/loop3         18432    18432         0 100% /snap/amazon-ssm-agent/1566\r\ntmpfs             396972        0    396972   0% /run/user/1000\r\n\r\n```\r\nWhat the system shows you here is the filesystem free diskspace that your machine has in 1Kb blocks, **But Ins\'t it hard to read ?** \r\n\r\n![alt text](https://zeitguysmedia.com/wp-content/uploads/2017/01/Email-Font-1280x720.jpg)\r\n\r\nfor me it is, so lets prettify it a bit. Type:\r\n\r\n```bash\r\nubuntu@ip-10-0-2-48:~/projects/thedevopsguy/thedevopsguy-rails/ci-cd/cd$ df -h\r\nFilesystem      Size  Used Avail Use% Mounted on\r\nudev            1.9G     0  1.9G   0% /dev\r\ntmpfs           388M  1.5M  387M   1% /run\r\n/dev/nvme0n1p1   30G   22G  7.2G  76% /\r\ntmpfs           1.9G     0  1.9G   0% /dev/shm\r\ntmpfs           5.0M     0  5.0M   0% /run/lock\r\ntmpfs           1.9G     0  1.9G   0% /sys/fs/cgroup\r\n/dev/loop0       94M   94M     0 100% /snap/core/9066\r\n/dev/loop1       94M   94M     0 100% /snap/core/8935\r\n/dev/loop2       18M   18M     0 100% /snap/amazon-ssm-agent/1455\r\n/dev/loop3       18M   18M     0 100% /snap/amazon-ssm-agent/1566\r\ntmpfs           388M     0  388M   0% /run/user/1000\r\n\r\n```\r\n\r\nNow, its better, in this report we have used 22 GB out of 30 GB of the `/dev/nvme0n1p1` file system ( which is essentially the mount point of the external disk) and leaving us 76% of the free disk.\r\n\r\n### So, Why do we need this ?\r\n\r\nAs a SysEngineer or DevOps guy you may need to control this amount effectively in order to not let your File system expanded over your hand and crash. A good example would be the logging system where you get tons of logs file daily and you should be controlling it by utilizing this command (or of course use some of the other metrics :D ). Then, you are able to wipe out the redundant data and keep your system nice and clean, for better sustainability.\r\n\r\n\r\n','2020-05-14 18:26:29','2020-06-25 07:14:54','https://wiki.base22.com/btg/files/33096359/33161277/1/1258778159000/unix-linux-df-command.png',NULL,244,'how-to-display-your-unix-linux-machine-available-diskspace-for-filesystem-by-df-h-command','approved'),(3,'How do I know which Linux version is my PC running on?','This article will show you how you can find your Linux version on your PC','In your terminal, type:\r\n\r\n```ruby\r\n$ cat /etc/os-release\r\n\r\nNAME=\"Ubuntu\"\r\nVERSION=\"18.04.2 LTS (Bionic Beaver)\"\r\nID=ubuntu\r\nID_LIKE=debian\r\nPRETTY_NAME=\"Ubuntu 18.04.2 LTS\"\r\nVERSION_ID=\"18.04\"\r\nHOME_URL=\"https://www.ubuntu.com/\"\r\nSUPPORT_URL=\"https://help.ubuntu.com/\"\r\nBUG_REPORT_URL=\"https://bugs.launchpad.net/ubuntu/\"\r\nPRIVACY_POLICY_URL=\"https://www.ubuntu.com/legal/terms-and-policies/privacy-policy\"\r\nVERSION_CODENAME=bionic\r\nUBUNTU_CODENAME=bionic\r\n\r\n```\r\n![alt_text](https://i.ibb.co/P47PJJW/image.png)\r\n\r\nSo, in this report you see that we have the name of the linux OS is `Ubuntu` and therefore it is `Ubuntu`, see ya champ!','2020-05-14 19:54:03','2020-06-26 06:40:22','https://i.ibb.co/P47PJJW/image.png',NULL,455,'how-do-i-know-which-linux-version-is-my-pc-running-on','approved'),(5,'How to create slug URLs in Rails from scratch','In this post I am going to share with you how to create slugs URLs from scratch in Ruby on Rails','## Introduction\r\nIn this post I will show you my way to make URLs in Rails display as `http://example.com/posts/12-name-of-your-post` instead of `http://example.com/posts/12`, which is better for looking and **SEO** in general\r\n\r\n#### First, we add the slug string field to the table that we want to.\r\n\r\n\r\n```bash\r\nrails generate migration AddSlugToPosts slug:string\r\n```\r\n#### Check the migration file, make sure that we add the right column to the right table\r\n\r\n\r\n```ruby\r\n# migration.rb\r\nclass AddSlugToPosts < ActiveRecord::Migration[5.2]\r\n  def change\r\n    add_column :posts, :slug, :string\r\n  end\r\nend\r\n```\r\n# Run migration command\r\n```bash\r\nrails db:migrate\r\n```\r\n#### Add a function to create slug from post\'s name in model\r\n\r\n\r\n```ruby\r\n# article.rb\r\nclass Article < ApplicationRecord\r\n  after_validation :set_slug, only: [:create, :update]\r\n\r\n  def to_param\r\n    \"#{id}-#{slug}\"\r\n  end\r\n  \r\n  private\r\n\r\n  def set_slug\r\n    self.slug = name.to_s.parameterize\r\n  end \r\nend\r\n```\r\n#### Update the existing records by\r\n\r\n\r\n```bash\r\n# rails console\r\nposts = Post.all\r\nposts.each do |post|\r\n    post.update(slug: post.name.parameterize)\r\nend\r\n```\r\n\r\nNow you can access the post by using slugs link\r\n\r\n![alt_image](https://i.ibb.co/2scq2FV/image.png)\r\n\r\n','2020-05-18 17:46:48','2020-06-28 00:24:45','https://i.pinimg.com/originals/b1/c5/0f/b1c50f2e2af9223035a629cc3a6138da.png',NULL,280,'how-to-create-slug-urls-in-rails-from-scratch','approved'),(6,'Set up Terraform with Azure - test table','This article show the way to set up Terraform to work with Azure','- [Heading](#heading)\r\n  * [Sub-heading](#sub-heading)\r\n    + [Sub-sub-heading](#sub-sub-heading)\r\n- [Heading](#heading-1)\r\n  * [Sub-heading](#sub-heading-1)\r\n    + [Sub-sub-heading](#sub-sub-heading-1)\r\n- [Heading](#heading-2)\r\n  * [Sub-heading](#sub-heading-2)\r\n    + [Sub-sub-heading](#sub-sub-heading-2)\r\n\r\n\r\n# Heading levels\r\n\r\n> This is a fixture to test heading levels\r\n\r\n<!-- toc -->\r\n\r\n## Heading\r\n\r\nThis is an h1 heading\r\n\r\n### Sub-heading\r\n\r\nThis is an h2 heading\r\n\r\n#### Sub-sub-heading\r\n\r\nThis is an h3 heading\r\n\r\n## Heading\r\n\r\nThis is an h1 heading\r\n\r\n### Sub-heading\r\n\r\nThis is an h2 heading\r\n\r\n#### Sub-sub-heading\r\n\r\nThis is an h3 heading\r\n\r\n## Heading\r\n\r\nThis is an h1 heading\r\n\r\n### Sub-heading\r\n\r\nThis is an h2 heading\r\n\r\n#### Sub-sub-heading\r\n\r\nThis is an h3 heading\r\n## Introduction\r\n\r\nIn this section I am going to show the steps to install **Terraform** and provision infrastructure on **Azure**.\r\n\r\n## Implementation.\r\n\r\n#### 1. Installing Terraform.\r\n\r\nFirst, we need terraform. You can go to this site: https://www.terraform.io/downloads.html and download the approriate version for your OS, either Windows, MacOS or Linux.\r\n\r\n![alt_image](https://i.ibb.co/FqPTn61/image.png)\r\n\r\nThen, you can verify the installation by checking the version:\r\n\r\n```bash\r\nterraform -v\r\n\r\n```\r\n![alt_image](https://i.ibb.co/J7jTCt1/image.png)\r\n\r\n#### 2. (Optional) Get an IDE for your Terraform coding.\r\n\r\nNext, you can get an IDE for coding, I would prefer Visual Studio Code (https://code.visualstudio.com/) for this task but you can choose what ever you want.\r\n\r\nAfter installing, you should install Terraform extensions to get your text highlighted and errors checked.\r\n\r\n![alt_image](https://i.ibb.co/8YZX7rr/image.png)\r\n\r\n#### 3. Set up files for Terraform provisioning.\r\n\r\nA typical Terraform folder will include, at a minimum, a **main.tf**, a **variables.tf**, and **outputs.tf** file (if required).\r\n\r\n1. [Introduction](#Introduction)','2020-05-25 07:50:13','2020-06-24 04:54:37','',NULL,20,'set-up-terraform-with-azure-test-table','pending');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idols`
--

DROP TABLE IF EXISTS `idols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `idols` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `astrological` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `debutyear` int(11) DEFAULT NULL,
  `haircolor` varchar(255) DEFAULT NULL,
  `eyecolor` varchar(255) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `measurement` varchar(255) DEFAULT NULL,
  `tattoos` varchar(255) DEFAULT NULL,
  `piercings` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `imageurl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idols`
--

LOCK TABLES `idols` WRITE;
/*!40000 ALTER TABLE `idols` DISABLE KEYS */;
/*!40000 ALTER TABLE `idols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `height` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tags_id` bigint(20) DEFAULT NULL,
  `idol_id` bigint(20) DEFAULT NULL,
  `tag_id` bigint(20) DEFAULT NULL,
  `album_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_images_on_article_id` (`article_id`),
  KEY `index_images_on_tags_id` (`tags_id`),
  KEY `index_images_on_idol_id` (`idol_id`),
  KEY `index_images_on_tag_id` (`tag_id`),
  KEY `index_images_on_album_id` (`album_id`),
  CONSTRAINT `fk_rails_028d35b473` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `fk_rails_136274ba55` FOREIGN KEY (`idol_id`) REFERENCES `idols` (`id`),
  CONSTRAINT `fk_rails_1d373e1cfb` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`),
  CONSTRAINT `fk_rails_733074f893` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`),
  CONSTRAINT `fk_rails_9076dbadb2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,1,'a1','https://image-english.vov.vn/w500/uploaded/wzzqbijjvws/2018_05_10/Cannes_1_MUZZ.JPG',NULL,NULL,'','2020-05-14 17:29:07','2020-05-14 17:29:07',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interactions`
--

DROP TABLE IF EXISTS `interactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `anohash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_interactions_on_article_id` (`article_id`),
  KEY `index_interactions_on_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interactions`
--

LOCK TABLES `interactions` WRITE;
/*!40000 ALTER TABLE `interactions` DISABLE KEYS */;
INSERT INTO `interactions` VALUES (17,3,NULL,'like','2020-05-18 19:10:22','2020-05-21 06:35:55','e0c7a07162c1f14ed50b8bfbd9a288f0'),(18,2,NULL,'like','2020-05-18 19:10:27','2020-05-25 03:06:41','e0c7a07162c1f14ed50b8bfbd9a288f0'),(19,1,NULL,'like','2020-05-18 19:10:44','2020-05-20 06:41:47','e0c7a07162c1f14ed50b8bfbd9a288f0'),(20,5,NULL,'like','2020-05-19 04:04:43','2020-05-22 07:42:57','e0c7a07162c1f14ed50b8bfbd9a288f0'),(21,5,NULL,'like','2020-05-20 08:30:26','2020-05-20 08:30:26','3806a4ac9a472ee7dedfdbb185a99679'),(22,5,NULL,'like','2020-05-21 04:13:13','2020-05-21 04:13:18','73c2dd81b2bab6215c26aafa6f757da7'),(23,5,NULL,'like','2020-05-29 18:32:05','2020-05-29 18:59:57','3dff6cb78a0b4f4ff110778209377ea4'),(24,3,NULL,'like','2020-05-29 19:00:01','2020-05-29 19:00:03','3dff6cb78a0b4f4ff110778209377ea4'),(25,2,NULL,'like','2020-05-29 21:16:11','2020-06-01 13:44:21','3dff6cb78a0b4f4ff110778209377ea4'),(26,1,NULL,'like','2020-05-30 09:37:45','2020-05-30 09:37:45','e12bbeba97f1b526ecf337a8bc9c094a'),(27,3,NULL,'like','2020-05-30 13:17:06','2020-05-30 13:17:08','2532b0a28b3fd754d8383484fcbfcd21'),(28,1,NULL,'like','2020-05-30 13:17:11','2020-05-30 13:17:11','2532b0a28b3fd754d8383484fcbfcd21'),(29,2,NULL,'like','2020-05-30 13:17:14','2020-05-30 13:17:14','2532b0a28b3fd754d8383484fcbfcd21'),(30,1,NULL,'like','2020-06-01 13:44:01','2020-06-01 13:44:01','3dff6cb78a0b4f4ff110778209377ea4'),(31,3,NULL,'like','2020-06-01 13:46:55','2020-06-01 13:46:59','89521547ce0ec38d6f8b7ff17c484d7e'),(32,5,NULL,'like','2020-06-02 13:18:06','2020-06-02 13:19:36','6024ff2b01009174eccbc89702578f69'),(33,5,NULL,'like','2020-06-03 10:38:54','2020-06-03 10:38:54','7fb7e7f4ca9728eae909c174c3f3917a'),(34,1,NULL,'like','2020-06-03 10:39:02','2020-06-03 10:39:02','7fb7e7f4ca9728eae909c174c3f3917a'),(35,3,NULL,'like','2020-06-03 11:51:33','2020-06-03 11:51:33','7fb7e7f4ca9728eae909c174c3f3917a'),(36,2,NULL,'like','2020-06-03 15:23:57','2020-06-03 15:23:57','7fb7e7f4ca9728eae909c174c3f3917a'),(37,2,NULL,'like','2020-06-04 04:12:02','2020-06-04 04:12:02','2056419150b125c4d7c9ea632ee44fa5'),(38,3,NULL,'like','2020-06-04 04:53:50','2020-06-04 04:53:50','2056419150b125c4d7c9ea632ee44fa5'),(39,5,NULL,'like','2020-06-04 06:45:00','2020-06-04 06:45:00','2056419150b125c4d7c9ea632ee44fa5'),(40,5,NULL,'like','2020-06-10 13:04:54','2020-06-10 13:04:54','e3ca4d2e44083dcc47e77cf5a3c270b4'),(41,1,NULL,'like','2020-06-10 13:05:02','2020-06-10 13:05:02','e3ca4d2e44083dcc47e77cf5a3c270b4'),(42,1,NULL,'like','2020-06-13 13:02:43','2020-06-13 13:02:43','1f4ab02b41c2c7f139fd4c9309a5ab58'),(43,2,NULL,'like','2020-06-16 07:36:50','2020-06-16 07:36:50','0d49e5f930b0e598f3743cf247b32671'),(44,1,NULL,'like','2020-06-16 09:57:27','2020-06-16 09:57:27','0d49e5f930b0e598f3743cf247b32671'),(45,5,NULL,'like','2020-06-16 09:57:32','2020-06-25 07:14:51','0d49e5f930b0e598f3743cf247b32671'),(46,3,NULL,'like','2020-06-19 06:07:15','2020-06-19 06:07:15','0d49e5f930b0e598f3743cf247b32671');
/*!40000 ALTER TABLE `interactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_item_categories`
--

DROP TABLE IF EXISTS `inventory_item_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_item_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_item_categories`
--

LOCK TABLES `inventory_item_categories` WRITE;
/*!40000 ALTER TABLE `inventory_item_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_item_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_models`
--

DROP TABLE IF EXISTS `inventory_models`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_models` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `model_name` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sold_quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_models`
--

LOCK TABLES `inventory_models` WRITE;
/*!40000 ALTER TABLE `inventory_models` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_models` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `cookies` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_orders_on_user_id` (`user_id`),
  CONSTRAINT `fk_rails_f868b47f6a` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20190111094255'),('20190111094426'),('20190111094651'),('20190111094842'),('20190111095121'),('20190111095204'),('20190111095331'),('20190111101349'),('20190111101659'),('20190111101852'),('20190111104800'),('20190111104826'),('20190111105039'),('20190111105103'),('20190111105959'),('20190112144555'),('20190113152932'),('20190113162544'),('20190114070428'),('20190114150649'),('20190114150651'),('20190115050408'),('20190115064128'),('20190115090654'),('20190115100914'),('20190115101515'),('20190115101518'),('20190115103805'),('20190115104200'),('20190115145015'),('20190115145509'),('20190115151330'),('20190116043024'),('20190130101737'),('20190404101337'),('20200423123648'),('20200423123911'),('20200423123912'),('20200423155301'),('20200423160018'),('20200423160105'),('20200423161730'),('20200423162050'),('20200423162213'),('20200424130827'),('20200429140540'),('20200429143922'),('20200429144037'),('20200501085455'),('20200501133915'),('20200501170435'),('20200501172744'),('20200502002834'),('20200502152508'),('20200502152610'),('20200502153926'),('20200502154046'),('20200502154319'),('20200502154417'),('20200502154504'),('20200502154547'),('20200502162128'),('20200502164443'),('20200502164726'),('20200502235011'),('20200503114642'),('20200506074806'),('20200509035009'),('20200515131017'),('20200517130954'),('20200517131057'),('20200518171349');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specific_items`
--

DROP TABLE IF EXISTS `specific_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specific_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `inventory_item_id` int(11) DEFAULT NULL,
  `serial_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_specific_items_on_serial_number` (`serial_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specific_items`
--

LOCK TABLES `specific_items` WRITE;
/*!40000 ALTER TABLE `specific_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `specific_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `logourl` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'Linux','Linux','2020-05-15 02:16:20','2020-05-18 18:03:35','','linux'),(2,'Ruby','','2020-05-15 02:16:30','2020-05-18 18:03:35','','ruby'),(3,'Ruby On Rails','','2020-05-15 02:16:43','2020-05-18 18:03:35','','ruby-on-rails'),(4,'Docker','','2020-05-15 02:16:50','2020-05-18 18:03:35','','docker'),(5,'C#','','2020-05-15 09:56:35','2020-05-18 18:03:35','','c'),(6,'.Net Core','','2020-05-15 09:57:05','2020-05-18 18:03:35','','net-core'),(7,'AWS SysOps','','2020-05-15 09:57:37','2020-05-18 18:03:35','','aws-sysops'),(8,'Azure','','2020-05-15 09:57:43','2020-05-18 18:03:35','','azure'),(9,'Kubernetes','','2020-05-15 17:25:26','2020-05-18 18:03:35','','kubernetes'),(12,'VueJS','','2020-05-17 07:19:38','2020-05-18 18:03:35','','vuejs'),(13,'Terraform','','2020-05-24 15:49:34','2020-05-24 15:49:34','','terraform');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,NULL,NULL,'2020-05-14 19:07:28','2020-05-14 19:07:28','$2a$11$rk3k10NG0gMe5RsHx6I5U.xGjjmMsvZlDXY5Q8tFbwmvPG0zUGfaC',NULL,NULL,NULL,'tuan@gmail.com'),(2,'admin@gmail.com',1,'2020-05-14 19:07:52','2020-05-14 19:10:58','$2a$11$19ch6VIYAzA889aau66Ps.UCj8KHaCAvQ0YQ1JzSn0BRSp2WjRg5u',NULL,NULL,NULL,'admin@gmail.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `stock_type` varchar(255) DEFAULT NULL,
  `exterior_color` varchar(255) DEFAULT NULL,
  `interior_color` varchar(255) DEFAULT NULL,
  `transmission` varchar(255) DEFAULT NULL,
  `drivetrain` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `miles` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_videos_on_article_id` (`article_id`),
  CONSTRAINT `fk_rails_ddcc67cbd8` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votes`
--

DROP TABLE IF EXISTS `votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `votable_type` varchar(255) DEFAULT NULL,
  `votable_id` int(11) DEFAULT NULL,
  `voter_type` varchar(255) DEFAULT NULL,
  `voter_id` int(11) DEFAULT NULL,
  `vote_flag` tinyint(1) DEFAULT NULL,
  `vote_scope` varchar(255) DEFAULT NULL,
  `vote_weight` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_votes_on_voter_id_and_voter_type_and_vote_scope` (`voter_id`,`voter_type`,`vote_scope`),
  KEY `index_votes_on_votable_id_and_votable_type_and_vote_scope` (`votable_id`,`votable_type`,`vote_scope`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votes`
--

LOCK TABLES `votes` WRITE;
/*!40000 ALTER TABLE `votes` DISABLE KEYS */;
/*!40000 ALTER TABLE `votes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-28  8:25:44
