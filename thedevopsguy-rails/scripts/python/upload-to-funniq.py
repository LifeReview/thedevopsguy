import requests 
import json
from slugify import slugify
from google.cloud import storage
# from firebase import firebase
import os
import urllib
from firebase_admin import storage
import firebase_admin
from firebase_admin import credentials
import base64

PROJECT_ID = "funniq-78dbd"
IS_EXTERNAL_PLATFORM = True # False if using Cloud Functions
BLOB_FOLDER = "funniq_production"
# firebase_app = "1:804986229551:web:a57e9b170a64fc23"
firebase_app = None
storage_client = None
DOWNLOADED_IMAGE_LINK = None

def uploadImageToFirebase(ImageName):  
  with open(ImageName, "rb") as file:
    url = "https://api.imgbb.com/1/upload"
    payload = {
        "key": "a55498bd48557cea1a52ba2cf46be514",
        "image": base64.b64encode(file.read()),
    }
    res = requests.post(url, payload)

    DOWNLOADED_IMAGE_LINK = res.json()

    DOWNLOADED_IMAGE_LINK = (DOWNLOADED_IMAGE_LINK["data"]["url"])

  return DOWNLOADED_IMAGE_LINK
  

for file in ['know_your_meme_articles.json','memedroid_articles.json']:
  with open(file) as json_file:
    print(file)
    data = json.load(json_file)
    url = 'https://backend.funniq.com/api/v1/articles/create_article'
    IMAGE_FOLDER = "funniq-images/"  

    print(len(data))

    for i in range(len(data) - 1):
      if not os.path.exists( IMAGE_FOLDER + slugify(data[i]["header"]) + ".jpg" ):
        try:
          urllib.urlretrieve(data[i]["imagelink"], IMAGE_FOLDER + slugify(data[i]["header"]) + ".jpg")
        except IOError:
          print("Cant download the image" + data[i]["imagelink"])
        else:
          DOWNLOADED_IMAGE_LINK = uploadImageToFirebase( IMAGE_FOLDER + slugify(data[i]["header"]) + ".jpg")
          
          payload = {
              "article":{
              "name": slugify(data[i]["header"]),
              "header": slugify(data[i]["header"]),
              "mediatype": "image",
              "url": DOWNLOADED_IMAGE_LINK,
              "category": "Fun",
              "status": "pending",
              }  
          }
          headers = {"X-USER-TOKEN": "3CBeT95ZtKnRfbpo3Zo56CA3"}
          r = requests.post(url, json=payload, headers=headers)
          print(r.status_code)
          print(r.content)
      else:
        print("Duplicated image " + IMAGE_FOLDER + slugify(data[i]["header"]) + ".jpg" )