import boto3
from botocore.exceptions import NoCredentialsError
import os.path
import os
import base64
import requests 
import json

# Set environment variables
ACCESS_KEY = os.getenv('AWS_ACCESS_KEY')
SECRET_KEY = os.getenv('AWS_SECRET_KEY')


print(ACCESS_KEY)

boto3.set_stream_logger('')

IDOL_NAME='rara-anzai'

ESPISODE='0'

OUTPUT='2'


def getVideoFileName(search_folder):
    for filename in os.listdir(search_folder):
        if filename.endswith(".mp4"):            
            print (filename)
            return filename
            continue
        else:
            print("mp4 file not found")
            continue

local_folder='/mnt/d/hd-files/videos/' + IDOL_NAME + '/' + IDOL_NAME + '-' + ESPISODE + '/output-' + OUTPUT
local_file=getVideoFileName(local_folder)
file_to_upload= local_folder + '/' + local_file
# local_file='upload-to-funniq.py'
bucket_name='niceporn'
s3_file_name='video-prod/' + IDOL_NAME + '/' + IDOL_NAME + '-' + ESPISODE + '/' + local_file



def uploadImageToImgbb(ImageName):  
  with open(ImageName, "rb") as file:
    url = "https://api.imgbb.com/1/upload"
    payload = {
        "key": "a55498bd48557cea1a52ba2cf46be514",
        "image": base64.b64encode(file.read()),
    }
    res = requests.post(url, payload)

    DOWNLOADED_IMAGE_LINK = res.json()

    DOWNLOADED_IMAGE_LINK = (DOWNLOADED_IMAGE_LINK["data"]["url"])

  return DOWNLOADED_IMAGE_LINK
  

def upload_to_aws(local_file, bucket, s3_file):
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)

    print(local_file)

    try:
        s3.upload_file(local_file, 
                        bucket, 
                        s3_file,
                        ExtraArgs={'ACL': 'public-read', 'ContentType': 'video/mp4'})
        print("Upload Successful")
        return True
    # except FileNotFoundError:
    #     print("The file was not found")
    #     return False
    except NoCredentialsError:
        print("Credentials not available")
        return False



if os.path.isfile(local_file):
    print ("File exist")
else:
    print ("File not exist")


# directory = 'the/directory/you/want/to/use'

uploaded = upload_to_aws(file_to_upload, bucket_name, s3_file_name)

for filename in os.listdir(local_folder):
    if filename.endswith(".jpg"):
        DOWNLOADED_IMAGE_LINK = uploadImageToImgbb(local_folder + "/" + filename)
        print (DOWNLOADED_IMAGE_LINK)
        continue
    else:
        continue

