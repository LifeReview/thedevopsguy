#!/bin/bash

#Run by this command
#bash create-thumbnails.sh $#INPUT_VIDEO $#OUTPUT_FILE

SOURCE_VIDEO=$1
DESTINATION_FOLDER=$2
DESTINATION_IMAGE_FILE=$3

END=4

function show_time () {    
    ((h=${1}/3600))
    ((m=(${1}%3600)/60))
    ((s=${1}%60))
    printf "%02d:%02d:%02d\n" $h $m $s
}

function divide_time () {    
    ((divided_time=${1}/${2}))
    printf "%01d\n" $divided_time
}

function video_length () {
    VIDEO_LENGTH=$(ffprobe -i ${1} -show_entries format=duration -v quiet -of csv="p=0")

    VIDEO_LENGTH=${VIDEO_LENGTH%\.*}
}

for i in $(seq 2 $END); do 
    video_length $1
    CUT_TIME=$(show_time $(divide_time $VIDEO_LENGTH $i))

    ffmpeg -i $SOURCE_VIDEO\
    -ss $CUT_TIME\
    -vframes 1\
    "$DESTINATION_FOLDER/$DESTINATION_IMAGE_FILE-$i.jpg"    
done

# video_length $1
# echo $VIDEO_LENGTH