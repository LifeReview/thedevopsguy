class AddAnohashToInteraction < ActiveRecord::Migration[5.2]
  def change
    add_column :interactions, :anohash, :string
  end
end
