class CreateArticleModelRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :article_model_records do |t|
      # t.references :article, foreign_key: true
      # t.references :model, foreign_key: true
      t.belongs_to :article
      t.belongs_to :model
      t.timestamps
    end
  end
end
