class AddAlbumToImage < ActiveRecord::Migration[5.2]
  def change
    add_reference :images, :album, foreign_key: true
  end
end
