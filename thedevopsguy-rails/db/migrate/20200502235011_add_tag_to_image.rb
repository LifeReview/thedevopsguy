class AddTagToImage < ActiveRecord::Migration[5.2]
  def change
    add_reference :images, :tag, foreign_key: true
  end
end
