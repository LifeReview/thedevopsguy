class CreateArticleIdolRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :article_idol_records do |t|
      # t.references :article, foreign_key: true
      # t.references :idol, foreign_key: true
      t.belongs_to :article
      t.belongs_to :idol
      t.timestamps
    end
  end
end
