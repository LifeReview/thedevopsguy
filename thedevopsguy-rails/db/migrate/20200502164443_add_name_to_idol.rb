class AddNameToIdol < ActiveRecord::Migration[5.2]
  def change
    add_column :idols, :name, :string
  end
end
