class AddTagRefToImage < ActiveRecord::Migration[5.2]
  def change
    add_reference :images, :tags, foreign_key: true
  end
end
