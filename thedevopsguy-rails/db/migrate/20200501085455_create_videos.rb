class CreateVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :videos do |t|
      t.references :article, foreign_key: true
      t.string :name
      t.string :url
      t.string :format
      t.integer :length

      t.timestamps
    end
  end
end
