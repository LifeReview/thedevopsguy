class CreateImages < ActiveRecord::Migration[5.2]
  def change
    create_table :images do |t|
      t.references :article, foreign_key: true
      t.string :name
      t.string :url
      t.string :height
      t.integer :width
      t.string :format

      t.timestamps
    end
  end
end
