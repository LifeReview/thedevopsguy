class AddVisitcountToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :visitcount, :integer
  end
end
