class AddVideoLinkToArticles < ActiveRecord::Migration[5.2]
  def change
    add_column :articles, :videolink, :string
  end
end
