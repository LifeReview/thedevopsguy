class ArticleModelRecordTable < ActiveRecord::Migration[5.2]
  def change
    def up
      drop_table :article_model_records
    end
  
    def down
      raise ActiveRecord::IrreversibleMigration
    end
  end
end
