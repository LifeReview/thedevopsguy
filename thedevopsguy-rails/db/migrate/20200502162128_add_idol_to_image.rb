class AddIdolToImage < ActiveRecord::Migration[5.2]
  def change
    add_reference :images, :idol, foreign_key: true
  end
end
