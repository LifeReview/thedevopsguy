class AddLogoUrlToTag < ActiveRecord::Migration[5.2]
  def change
    add_column :tags, :logourl, :string
  end
end
