class CreateInteractions < ActiveRecord::Migration[5.2]
  def change
    create_table :interactions do |t|
      # t.references :article, foreign_key: true
      # t.references :user, foreign_key: true
      t.belongs_to :article
      t.belongs_to :user
      t.string :status

      t.timestamps
    end
  end
end
