class AddImageurlToIdol < ActiveRecord::Migration[5.2]
  def change
    add_column :idols, :imageurl, :string
  end
end
