class CreateModels < ActiveRecord::Migration[5.2]
  def change
    create_table :models do |t|
      t.string :alias
      t.string :country
      t.string :city
      t.string :astrological
      t.date :birthday
      t.integer :debutyear
      t.string :haircolor
      t.string :eyecolor
      t.integer :height
      t.string :measurement
      t.string :tattoos
      t.string :piercings

      t.timestamps
    end
  end
end
