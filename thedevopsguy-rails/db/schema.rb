# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_18_171349) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.integer "resource_id"
    t.string "author_type"
    t.integer "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "albums", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "type"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "article_idol_records", force: :cascade do |t|
    t.integer "article_id"
    t.integer "idol_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_article_idol_records_on_article_id"
    t.index ["idol_id"], name: "index_article_idol_records_on_idol_id"
  end

  create_table "article_tag_records", force: :cascade do |t|
    t.integer "article_id"
    t.integer "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_article_tag_records_on_article_id"
    t.index ["tag_id"], name: "index_article_tag_records_on_tag_id"
  end

  create_table "articles", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "videolink"
    t.integer "visitcount"
    t.string "slug"
    t.string "status"
  end

  create_table "idols", force: :cascade do |t|
    t.string "alias"
    t.string "country"
    t.string "city"
    t.string "astrological"
    t.date "birthday"
    t.integer "debutyear"
    t.string "haircolor"
    t.string "eyecolor"
    t.integer "height"
    t.string "measurement"
    t.string "tattoos"
    t.string "piercings"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "imageurl"
  end

  create_table "images", force: :cascade do |t|
    t.integer "article_id"
    t.string "name"
    t.string "url"
    t.string "height"
    t.integer "width"
    t.string "format"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tags_id"
    t.integer "idol_id"
    t.integer "tag_id"
    t.integer "album_id"
    t.index ["album_id"], name: "index_images_on_album_id"
    t.index ["article_id"], name: "index_images_on_article_id"
    t.index ["idol_id"], name: "index_images_on_idol_id"
    t.index ["tag_id"], name: "index_images_on_tag_id"
    t.index ["tags_id"], name: "index_images_on_tags_id"
  end

  create_table "interactions", force: :cascade do |t|
    t.integer "article_id"
    t.integer "user_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "anohash"
    t.index ["article_id"], name: "index_interactions_on_article_id"
    t.index ["user_id"], name: "index_interactions_on_user_id"
  end

  create_table "inventory_item_categories", force: :cascade do |t|
    t.string "category_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
  end

  create_table "inventory_models", force: :cascade do |t|
    t.string "inventory_model_name"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.integer "sold_quantity"
  end

  create_table "order_line_items", force: :cascade do |t|
    t.integer "order_id"
    t.integer "order_item_qty", default: 1
    t.float "total_price"
    t.float "order_line_item_price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "fixed_item_price"
    t.index ["order_id"], name: "index_order_line_items_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "user_id"
    t.float "total_amount"
    t.integer "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cookies"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "specific_items", force: :cascade do |t|
    t.string "serial_number", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["serial_number"], name: "index_specific_items_on_serial_number", unique: true
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.integer "user_id_id"
    t.integer "faculty_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["faculty_id"], name: "index_students_on_faculty_id"
    t.index ["user_id_id"], name: "index_students_on_user_id_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "logourl"
    t.string "slug"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.boolean "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "email", default: "", null: false
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "title"
    t.string "stock_type"
    t.string "exterior_color"
    t.string "interior_color"
    t.string "transmission"
    t.string "drivetrain"
    t.integer "price"
    t.integer "miles"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "videos", force: :cascade do |t|
    t.integer "article_id"
    t.string "name"
    t.string "url"
    t.string "format"
    t.integer "length"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_videos_on_article_id"
  end

  create_table "votes", force: :cascade do |t|
    t.string "votable_type"
    t.integer "votable_id"
    t.string "voter_type"
    t.integer "voter_id"
    t.boolean "vote_flag"
    t.string "vote_scope"
    t.integer "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"
  end

end
