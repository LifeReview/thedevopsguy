# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'

run Rails.application

# require 'rack/rewrite'

# if ENV['RACK_ENV'] == 'development'
#   ENV['SITE_URL'] = 'localhost:3000'
# elsif ENV['RACK_ENV'] == 'test' 
#   ENV['SITE_URL'] = 'test.javlv.net'
# elsif ENV['RACK_ENV'] == 'production'    
#   ENV['SITE_URL'] = 'javlv.net'
# end

# use Rack::Rewrite do
#   r301 %r{.*}, "http://#{ENV['SITE_URL']}$&", :if => Proc.new { |rack_env|
#   rack_env['SERVER_NAME'].start_with?('www')}

#   r301 %r{^(.+)/$}, '$1'
    # r301 %r{.*}, 'https://test.javlv.net$&', :if => Proc.new {|rack_env|
    #     rack_env['SERVER_NAME'].start_with?('www')}
    