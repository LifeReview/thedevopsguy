ls -a

let A=$(date +%M)
let A=A/30*30
B=$(date +"%y%m%d_%H%M%S")$A

PROJECT_NAME="thedevopsguy-rails"
ENV_CURRENT="prod"
REMOTE_DEPLOY_SERVER="52.21.18.76"
PREFIX="develop"
REGISTRY="704088132487.dkr.ecr.ap-southeast-1.amazonaws.com/funniq"
PRO_ENV="$PROJECT_NAME-$ENV_CURRENT-"
TAG=$PROJECT_NAME"_"$ENV_CURRENT"-"$B

# aws configure set aws_access_key_id $1
# aws configure set aws_secret_access_key $2


###############################
IMAGE_NAME=$PROJECT_NAME-$ENV_CURRENT
echo "Image name : $IMAGE_NAME"
echo "Change Docker file was success"
##########################################
#
# echo $C > $FILE_PATH/$FILE_NAME

echo "tag-name: "  $TAG

#Compile to minify assets before builds
#do this first
# SECRET_KEY_BASE=123123414124rwqerqr
# export SECRET_KEY_BASE 
#SECRET_KEY_BASE=$(printenv SECRET_KEY_BASE)

#SECRET_KEY_BASE=$SECRET_KEY_BASE rake assets:precompile RAILS_ENV=production

#OK
docker build --file ./Dockerfile  -t $REGISTRY:$TAG .
# --no-cache=false
eval $(aws ecr get-login --region ap-southeast-1 --no-include-email --profile default)

docker push $REGISTRY:$TAG

# Add image tag to .env file
sed -i "1s@.*@THEDEVOPSGUY_RAILS_IMAGE=${REGISTRY}:${TAG}@"  ./ci_cd/ci/version.txt
