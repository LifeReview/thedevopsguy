require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module HDProject
  class Application < Rails::Application
    # before_action :set_locale_from_domain
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    
    # config.middleware.insert_after ActionDispatch::Static, Rack::Deflater
    config.middleware.use Rack::Deflater
    config.i18n.default_locale = :en
    # config.i18n.available_locales = [:en, :pl]

    config.exceptions_app = self.routes

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.     

    # config.hosts = {

    #   pt: ENV.fetch('HOST_PT', 'portuguese-domain.net'),
  
    #   de: ENV.fetch('HOST_DE', 'german-domain.de'),
  
    #   en: ENV.fetch('HOST_EN', 'english-domain.co.uk')
    # }
    # private

    # def set_locale_from_domain
  
    #   detected_locale = Rails.application.config.hosts.key(request.host)
  
    #   I18n.locale = detected_locale.present? ? detected_locale : I18n.default_locale
  
    # end
  end

  
end
