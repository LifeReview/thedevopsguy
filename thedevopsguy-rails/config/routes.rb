Rails.application.routes.draw do
  resources :albums
  # devise_scope :user do
  #   get "/signin" => "devise/sessions#new", as: "new_user_session" # custom path to login/sign_in
  #   get "/signup" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
  # end  

  # devise_for :users, :controllers => { :registrations => 'registrations' }, 
  #                     :path => 'accounts', 
  #                     :path_names =>{ :sign_in => 'login', 
  #                                     :sign_up => 'new', 
  #                                     :sign_out => 'logout', 
  #                                     :password => 'secret', 
  #                                     :confirmation => 'verification' }

  devise_scope :user do
    get "/signin" => "devise/sessions#new", as: "new_new_user_session"
    get "/signup" => "devise/registrations#new", as: "new_user_registration"
  end
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  


  resources :interactions do
    collection do
      post 'upvote' => 'interactions#upvote'
      post 'downvote' => 'interactions#downvote'
    end
  end
  resources :article_idol_records
  resources :idols
  resources :article_model_records
  resources :models
  resources :images
  resources :videos
  resources :article_tag_records
  resources :tags
  resources :vehicles do
    match '/scrape', to: 'vehicles#scrape', via: :post, on: :collection
  end
  
  
  ActiveAdmin.routes(self)
  resources :students
  namespace :admin_lte do
    resources :users
    resources :articles
    resources :inventory_items
    resources :inventory_item_categories
    resources :inventory_items
    resources :specific_items
    get '/home' => 'application#home'
    # Admin root
    root to: 'application#index'
  end

  # get '/admin' => 'static_pages#home'
  
  resources :articles, param: :slug
  resources :orders
  resources :order_line_items
  
  resources :users
  resources :inventory_items
  resources :inventory_models
  resources :inventory_item_categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html 
  get '/gallery' => 'static_pages#gallery'
  get '/bmi' => 'static_pages#bmi'
  get '/home' => 'static_pages#home'
  get '/help' => 'static_pages#help'
  get '/about_us' => 'static_pages#about_us'
  get '/contact' => 'static_pages#contact'
  get '/shopping_cart' => 'static_pages#shopping_cart'
  get '/categories' => 'inventory_item_categories#index'
  get '/checkout' => 'static_pages#checkout'
  get '/donate' => 'static_pages#donate'
  post '/interactions/create' => 'interactions#create', :via => :interactions
  #get '/articles' => 'static_pages#articles'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
  match "/404", to: "errors#file_not_found", via: :all
  match "/422", to: "errors#unprocessable", via: :all
  match "/500", to: "errors#internal_server_error", via: :all
end
