ActiveAdmin.register Album do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :name, :description, :type, :status, :image_id


show do
    attributes_table do
      row :name
      row :description
      row :type
      row :status
      row :image_id do |a|
        a.images
      end
    end
  end
  
  form do |f|
      f.inputs "Add/Edit Tag" do
        f.input :name
        f.input :description
        f.input :type
        f.input :status
      end
      actions
    end
  

end
