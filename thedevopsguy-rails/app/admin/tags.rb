ActiveAdmin.register Tag , as: "Tagss" do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :name, :description, :logourl, :image_id


show do
    attributes_table do
      row :name
      row :description
      row :logourl
      row :image_id do |a|
        a.image
      end
    end
  end
  
  form do |f|
      f.inputs "Add/Edit Tag" do
        f.input :name
        f.input :description
        f.input :logourl
      end
      actions
    end
  

end
