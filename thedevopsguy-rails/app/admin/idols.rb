ActiveAdmin.register Idol do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
permit_params  :alias, :country, :city, :astrological, :birthday,:debutyear,:haircolor,:eyecolor,:height, :measurement,:tattoos,:piercings, :name, :imageurl


show do
    attributes_table do
      row :name
      row :alias
      row :imageurl
      row :country
      row :city
      row :astrological
      row :birthday
      row :debutyear
      row :haircolor
      row :eyecolor
      row :height
      row :measurement
      row :tattoos
      row :piercings
    #   row :article_id do |a|
    #     a.article.id
    #   end
      
    end
  end
  
  form do |f|
      f.inputs "Add/Edit Tag" do
        f.input :name
        f.input :alias
        f.input :imageurl
        f.input :country, as: :select, collection: country_dropdown
        f.input :city
        f.input :astrological
        f.input :birthday
        f.input :debutyear
        f.input :haircolor
        f.input :eyecolor
        f.input :height
        f.input :measurement
        f.input :tattoos
        f.input :piercings
      end
      actions
    end
  

end
