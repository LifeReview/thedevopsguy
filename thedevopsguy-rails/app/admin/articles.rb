ActiveAdmin.register Article do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
# scope :all, default: true
permit_params :name, :description, :content, :image, :videolink, :visitcount, :status, :tag_ids


show do
    attributes_table do
      row :id
      row :name
      row :description
      row :content
      row :image
      # row :videolink
      row :status
      row :tag_ids do |a|
        a.tags
      end
    end
  end
  
  form do |f|
      f.inputs "Add/Edit Article" do
        f.input :name
        f.input :description
        f.input :content
        f.input :image
        # f.input :videolink        
        f.input :visitcount
        f.input :status, :as => :select, :collection => ["approved","pending","cancelled"]
        f.input :tag_ids, as: :select, collection: Tag.order(:name), label_method: :name, input_html: {multiple: true}
      end
      actions
    end
  

end
