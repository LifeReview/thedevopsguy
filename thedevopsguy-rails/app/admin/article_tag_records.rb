ActiveAdmin.register ArticleTagRecord do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :tag_id, :article_id


show do
    attributes_table do
        row :tag_id do |a|
            a.tag
        end
      
        row :article_id do |a|
            a.article
        end        
    end
  
  form do |f|
      f.inputs "Add/Edit ArticleTagRecord" do
        f.input :article, :as => :select, :collection => Article.all.ids
        f.input :tag, :as => :select, :collection => Tag.all.ids
      end
      actions
    end
end
end

