ActiveAdmin.register ArticleIdolRecord do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :idol_id, :article_id


show do
    attributes_table do
        row :idol_id do |a|
            a.idol
        end
      
        row :article_id do |a|
            a.article
        end        
    end
  
  form do |f|
      f.inputs "Add/Edit ArticleIdolRecord" do
        f.input :article, :as => :select, :collection => Article.all.ids
        f.input :idol, :as => :select, :collection => Idol.all.ids
      end
      actions
    end
end
end

