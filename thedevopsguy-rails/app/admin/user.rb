ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :name, :email, :admin, :password, :password_confirmation


show do
    attributes_table do
      row :name
      row :email
      # row :article_id do |a|
      #   a.article.id
      # end
      row :admin
    end
  end
  
  form do |f|
      f.inputs "Add/Edit Tag" do
        f.input :name
        f.input :email
        f.input :admin     
        f.input :password
        f.input :password_confirmation     
        # f.input :article, :as => :select, :collection => Article.all.ids
      end
      actions
    end
  

end
