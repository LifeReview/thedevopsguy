ActiveAdmin.register Image do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :name, :article_id, :url, :format, :length, :width, :idol_id, :tag_id, :album_id

show do
    attributes_table do
      row :name
      row :url
      # row :article_id do |a|
      #   a.article.id
      # end
      row :tag_id do |a|
        a.tag
      end
      row :album_id do |a|
        a.album
      end
      row :format
      row :length
      row :width
    end
  end
  
  form do |f|
    f.inputs "Add/Edit Tag" do
      f.input :name
      f.input :url
      f.input :format
      # f.input :length
      f.input :width
      f.input :article, :as => :select, :collection => Article.all.ids
      f.input :idol, :as => :select, :collection => Idol.all.ids
      f.input :tag, :as => :select, :collection => Tag.all.ids
      f.input :album, :as => :select, :collection => Album.all.ids
    end
    actions
  end
  

end
