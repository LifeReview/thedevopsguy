ActiveAdmin.register Interaction do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :user_id, :article_id, :anohash, :status


show do
    attributes_table do      
      row :article_id do |a|
        a.article.id
      end
      row :user_id do |a|
        a.user
      end
      row :anohash
      row :status
    end
  end
  
  form do |f|
      f.inputs "Add/Edit Tag" do
        f.input :article, :as => :select, :collection => Article.all.ids
        f.input :user, :as => :select, :collection => User.all.ids
        f.input :anohash
        f.input :status, :as => :select, :collection => ["like", "dislike", "neutral"]
      end
      actions
    end
  

end
