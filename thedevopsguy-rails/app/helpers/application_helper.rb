module ApplicationHelper

  class CodeRayify < Redcarpet::Render::HTML
    def block_code(code, language)
      CodeRay.scan(code, language).div
    end
    def image(link, title, alt_text)      
      parts = link.split(" ")
      link = parts[0]
  
      if parts.size == 1
        # ![alt](url.png)
        # %(<img src="#{link}" title="#{title}" alt="#{alt_text}">)        
        %(<img src="#{link}" width="100%" height="auto" title="#{title}" alt="#{alt_text}">)        
      else
        w, h = parts[1].gsub("=", "").gsub("px", "").split("x")
        if h
          # ![alt](url.png =500x200)
          %(<img src="#{link}" width="#{w}" height="#{h}" alt="#{alt_text}">)
        else
          # ![alt](url.png =500x)
          %(<img src="#{link}" width="#{w}" alt="#{alt_text}">)
        end
      end
    end
  end

  def cart_count_over_one
  	#binding.pry
    if @order != nil
      if @order.order_line_items.count > 0
        #binding.pry
        return "<span class='tag is-dark'>#{@order.order_line_items.sum(:order_item_qty)}</span>".html_safe
        #render html: @cart.order_line_items.count
      end
    end
  end

  def cart_has_items
    return @order.order_line_items.count > 0
  end	
 
  def tell_time_in_words(article)
    time_dif = Time.now - article.created_at
    if 1<= time_dif && time_dif  <= 60
      return time_dif.ceil.to_s + " seconds ago"
    elsif 60 < time_dif && time_dif  <= 3600
      return (time_dif/60).ceil.to_s + " minutes ago"
    elsif 3600 < time_dif && time_dif  <= 86400
      return (time_dif/3600).ceil.to_s + " hours ago"
    elsif 86400 < time_dif && time_dif  <= 604800
      return (time_dif/86400).ceil.to_s + " days ago"
    elsif 604800 < time_dif && time_dif  <= 2592000
      return (time_dif/604800).ceil.to_s + " weeks ago"
    elsif 2592000 < time_dif && time_dif  <= 31536000
      return (time_dif/2592000).ceil.to_s + " months ago"
    elsif 31536000 < time_dif && time_dif  <= 63072000
      return  (time_dif/31536000).round(1).to_s + " year ago"
    else
      return (time_dif/31536000).ceil.to_s + " years ago"
    end
  end

  def calculate_likes(article)
    return article.interactions.where(status: "like").count
  end

  def markdown(text)
    coderayified = CodeRayify.new(:filter_html => true, 
                                  :hard_wrap => true,
                                  :with_toc_data => true)
    options = {
      :fenced_code_blocks => true,
      :no_intra_emphasis => true,
      :autolink => true,
      :lax_html_blocks => true,
      :prettify => true,
      :codespan => true,
    }
    markdown_to_html = Redcarpet::Markdown.new(coderayified, options)
    markdown_to_html.render(text).html_safe
    
  end
  
  def table_of_contents(page)
        
    if config.markdown_engine == :redcarpet && config.markdown[:with_toc_data]
      renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML_TOC,
                                         fenced_code_blocks: true,
                                         xhtml: true)
      file = ::File.read(page.source_file)
      tocpre = '<nav><h2>Table of contents</h2>'
      tocpost = '</nav>'
      # ignore YAML frontmatter
      file = file.gsub(/^(---\s*\n.*?\n?)^(---\s*$\n?)/m, '')
      file = file.gsub(' & ', ' &amp; ')
      # Switch to ordered lists
      toc = renderer.render(file)
      toc = toc.sub('<ul>', '<ol property="dcterms:tableofContents">')
      toc = toc.gsub('<ul>', '<ol>')
      toc = toc.gsub('</ul>', '</ol>')
      tocpre + toc + tocpost unless toc.empty?
    end # End check for Redcarpet and toc_data
  end
end
