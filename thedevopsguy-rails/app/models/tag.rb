class Tag < ApplicationRecord
  after_validation :set_slug, only: [:create, :update]
  has_many :article_tag_records
  has_one :image    
  has_many :articles, :through => :article_tag_records
  
  def to_param
    "#{id}-#{slug}"
  end
    
  private

  def set_slug
    self.slug = name.to_s.parameterize
  end     
end
