class Image < ApplicationRecord
  belongs_to :article, optional: true
  belongs_to :idol, optional: true
  belongs_to :tag, optional: true
  belongs_to :album, optional: true
end
