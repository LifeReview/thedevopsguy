class Article < ApplicationRecord
  # belongs_to :students
  # mount_uploader :image, ImageUploader  # carrierwave support for our image column
  # serialize :image, JSON # If you use SQLite, add this line.
  after_validation :set_slug, only: [:create, :update]

  has_many :interactions, :dependent => :destroy
  has_many :article_tag_records, :dependent => :destroy
  has_many :tags, :through => :article_tag_records
  has_one :video, :dependent => :destroy
  
  has_many :article_idol_records, :dependent => :destroy
  has_many :idols, :through => :article_idol_records
  # has_one :image
  # has_many :tags, :through => article_tag_records

  def to_param
    "#{id}-#{slug}"
    # [id, slug.parameterize.truncate(80, omission: "")].join("-")
  end

  def increase_visit    
    self.visitcount += 1    
    save!
  end

  def tag_list
    tags.map(&:name).join(', ')
  end

  def tag_list=(names)
    self.tags = names.split(',').map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end

  private

  def set_slug
    self.slug = name.to_s.parameterize
  end 
end
