class Idol < ApplicationRecord
    has_one :image
    has_many :article_idol_records
    has_many :articles, :through => :article_idol_records
    
    include PgSearch
    pg_search_scope :search_by_full_name, against: [:name]
end
