class ErrorsController < ApplicationController

  def file_not_found    
    render status: 404, layout: "error404.html.haml"    

  end

  def unacceptable
    render status: 422
  end

  def internal_server_error
    render status: 500
  end

  end