class ImagesController < InheritedResources::Base

  private

    def image_params
      params.require(:image).permit(:article_id, :name, :url, :height, :width, :format)
    end
end

