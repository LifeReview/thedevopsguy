class StaticPagesController < ApplicationController
  # before_action :authenticate_user!, only: [:checkout]  

  def home  	
    @articles = Article.where(status: "approved").order(created_at: :DESC).page(params[:page]).per(8)
    # @articles = Article.order(created_at: :DESC).page(params[:page]).per(8)
    @tags = Tag.all
    set_meta_tags title: "Devops tricks and tips",
      description: 'This is the page to share the things that I learn as a DevOps engineer,
                    cover topics about Software Development, Operations and some Arts',
      keywords: 'DevOps, Software, Software Development, Operation, Arts, Personal, Blog'  	
  end

  def gallery
    render :layout => "custom/gallery"
  end

  def bmi
    @data = {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
            label: "My BMI",
            # backgroundColor: "rgba(151,187,205,0.2)",
            backgroundColor: "rgba(151,187,205,0.2)",
            borderColor: "rgba(255, 0, 0)",
            hoverBorderWidth: 100,
            # hoverBorderColor: "white",
            # pointHoverBorderColor: "white",
            pointHoverBorderWidth: "10",
            borderWidth: 4,
            hoverBackgroundColor: "white",            
            data: [32, 33, 34, 35, 36, 37, 38]
        }
      ]
    }

    @options ={
      height: "auto",
      class: "trainee-bmi-graph",
      aspectRatio: 2,
      maintainAspectRatio: false,
      responsive: true,
				title: {
					display: true,
					text: 'My BMI'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Day'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'BMI'
						}
					}]
				}    
    }
    render :layout => 'bmi'
  end

  def help
  end

  def about_us
  end

  def contact
  end

  def donate
    render :layout => 'donate'
  end

  def categories
    @inventory_item_categories = InventoryItemCategory.all
  end

  def set_cookies
    cookies[:user_name]   = "Horst Meier" 
    cookies[:customer_number] = "1234567890" 
  end
  
  def show_cookies
    @user_name    = cookies[:user_name]
    @customer_number = cookies[:customer_number]
  end
  
  def delete_cookies
    cookies.delete :user_name
    cookies.delete :customer_number
  end

  def checkout
    
  end
end