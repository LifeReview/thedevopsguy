class ArticlesController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  
  # GET /articles
  # GET /articles.json
  def index
    # @static_pages = InventoryItem.page(params[:page]).per(20)  
    set_meta_tags title: 'Post main page',
              description: 'Post main page.',
              keywords: 'Site, Main, Page, Show'
    
    @articles = Article.page(params[:page]).per(10)
    @articles_in_order = Article.all.order("created_at desc").limit(5)
    @search = params["search"]
    @tags = Tag.all
    if @search.present?
      @name = @search["name"]
      @articles = Article.where("name LIKE ?", "%#{@name}%")
    end    
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    set_meta_tags title: @article.name,
              description: 'Show the page in single.',
              keywords: 'Site, single, Page, Show'

    @articles_in_order = Article.all.order("created_at desc").limit(5)
    @all_articles = Article.all.page(params[:page]).per(8)
    # @video_tags = Article.joins(:memberships).where('memberships.team_id' => team_ids).where(['users.id != ?', self.id]).select('distinct users.*')
    @video_tags = Tag.joins(:article_tag_records).where( "article_tag_records.article_id" => @article.id).uniq
    # Article.joins(:article_tag_records).where( "article_tag_records.tag_id" => @tag.id).uniq
    @article.increase_visit
    # evaluate the condition
    # @section = (rand*10).to_i
    @section = 1

    if user_signed_in?
      interaction_status = @article.interactions.where(:user_id => current_user.id)[0].status
    elsif !cookies[:anonymous_user].nil?
      if @article.interactions.where(:anohash => cookies[:anonymous_user]).any?      
        interaction_status = @article.interactions.where(:anohash => cookies[:anonymous_user])[0].status
      end
    end

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: {
        article: @article,
        interaction_status: interaction_status,
        like_count: @article.interactions.where(:status => "like").count - @article.interactions.where(:status => "dislike").count
      }}
    
    end    
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:slug].to_i)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:name, :description, :content, :image, :search,:tag_list, tag_ids:[])
    end
end
