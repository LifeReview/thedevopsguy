class ArticleIdolRecordsController < InheritedResources::Base

  private

    def article_idol_record_params
      params.require(:article_idol_record).permit(:article_id, :idol_id)
    end
end

