class ArticleTagRecordsController < InheritedResources::Base

  private

    def article_tag_record_params
      params.require(:article_tag_record).permit(:article_id, :tag_id)
    end
end

