class InteractionsController < InheritedResources::Base
  before_action :set_interaction, only: [:show, :edit, :update, :destroy]
  respond_to :html, :js, :json
  

  def upvote            
    existingInteraction = Interaction.where(:article_id => interaction_params[:article_id]).where(:anohash =>  interaction_params[:anohash])

    if existingInteraction.any?      
      @interaction = existingInteraction[0]
      if (@interaction.status == "dislike")        
        @interaction.status = "like"
        @interaction.save
        # if @interaction.save
        #   render json: { interaction_status: @interaction.status }
        # end
      elsif (@interaction.status == "neutral")        
        @interaction.status = "like"
        @interaction.save
        # if @interaction.save
        #   render json: { interaction_status: @interaction.status }
        # end
      elsif (@interaction.status == "like")
        @interaction.status = "neutral"
        @interaction.save        
      end
    else
      @interaction = Interaction.new(interaction_params)        
      @interaction.save
      # if @interaction.save
      #   render json: { interaction_status: @interaction.status }
      # end
    end  
  end

  def downvote            
    existingInteraction = Interaction.where(:article_id => interaction_params[:article_id]).where(:anohash =>  interaction_params[:anohash])

    if existingInteraction.any?
      @interaction = existingInteraction[0]
      if (@interaction.status == "like")        
        @interaction.status = "dislike"
        @interaction.save
        # if @interaction.save
        #   render json: { interaction_status: @interaction.status }
        # end
      elsif (@interaction.status == "neutral")        
        @interaction.status = "dislike"
        @interaction.save
        # if @interaction.save
        #   render json: { interaction_status: @interaction.status }
        # end
      elsif (@interaction.status == "dislike")
        @interaction.status = "neutral"
        @interaction.save
        # binding.pry
      end
    else
      @interaction = Interaction.new(interaction_params)        
      @interaction.save
      # if @interaction.save
      #   render json: { interaction_status: @interaction.status }
      # end
    end    
    # render json: { interaction_status: @interaction.status }
    
  end
  # GET /interactions
  # GET /interactions.json
  def index
    @interactions = Interaction.all
  end

  # GET /interactions/1
  # GET /interactions/1.json
  def show
  end

  # GET /interactions/new
  def new
    @interaction = Interaction.new
  end

  # GET /interactions/1/edit
  def edit
  end

  # POST /interactions
  # POST /interactions.json
  def create
    
    # binding.pry
    
    @interaction = Interaction.new(interaction_params)

    respond_to do |format|
      if @interaction.save
        # format.html { redirect_to @interaction, notice: 'Interaction was successfully created.' }
        # format.json { render :show, status: :created, location: @interaction }
        format.js
      else
        format.html { render :new }
        format.json { render json: @interaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /interactions/1
  # PATCH/PUT /interactions/1.json
  def update
    respond_to do |format|
      if @interaction.update(interaction_params)
        format.html { redirect_to @interaction, notice: 'Interaction was successfully updated.' }
        format.json { render :show, status: :ok, location: @interaction }
      else
        format.html { render :edit }
        format.json { render json: @interaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /interactions/1
  # DELETE /interactions/1.json
  def destroy
    @interaction.destroy
    respond_to do |format|
      format.html { redirect_to interactions_url, notice: 'Interaction was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_interaction
      @interaction = Interaction.find(params[:id])
    end


    def interaction_params
      params.require(:interaction).permit(:article_id, :interaction_id, :status, :anohash)
    end
end

