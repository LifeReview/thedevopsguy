class VideosController < InheritedResources::Base

  private

    def video_params
      params.require(:video).permit(:article_id, :name, :url, :format, :length)
    end
end

