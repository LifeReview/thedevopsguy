class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include CurrentOrder
  # before_action :set_order
  before_action :set_anonymous_user_cookie
  before_action :set_tags_nav, :set_right_article_nav, :set_locale
  skip_before_action :verify_authenticity_token
  # before_action :set_locale
  # before_action :smart_backlink_session 

  #before_action :authenticate_user!, except: [:home, :articles]  
  #helper_method :current_order

  #def current_order
  #  if !session[:order_id].nil?
  #    Order.find(session[:order_id])
  #  else
  #    Order.new
  #  end
  #  binding.pry
  #end
  private
  
  def set_locale
    I18n.locale = I18n.default_locale
  end

  def set_anonymous_user_cookie
    if user_signed_in?
      # if cookies[:visited] == nil
      #   cookies[:visited] = ""
      #   cookies[:visited] = cookies[:visited] + "User:" + current_user.id.to_s + ";" + request.path + ";" + Time.now.in_time_zone("Hanoi").strftime("%m/%d/%y %I:%M %p") + ";"                
      # else
      #   cookies[:visited] = cookies[:visited] + "User:" + current_user.id.to_s + ";" + request.path + ";" + Time.now.in_time_zone("Hanoi").strftime("%m/%d/%y %I:%M %p") + ";"                
      # end
      # cookies[:anonymous_user] = " "
      cookies.delete :anonymous_user
    else
      if cookies[:anonymous_user] == nil
        cookies[:anonymous_user] = SecureRandom.hex
      end
    end
  end

  def smart_backlink_session
    # Clear all other links when on admin page - my preference
    delete_session_paths_visited_if_on_admin
    
    route_name = Rails.application.routes.router.recognize(request) { |route, _| route.name }.flatten.last.name
    
    # This global variable will be passed to our views
    # @path_name = "#{request.env['ORIGINAL_FULLPATH']}|#{route_name}"
    # @path_name = "#{request.env['ORIGINAL_FULLPATH']}|#{route_name}"

    @path_name = "#{request.original_fullpath}|#{route_name}"


    # We keep track on all page visited
    # Do not add same path more than once in the array ie if user refreshes browser
    session[:paths_visited] << @path_name if session[:paths_visited] and !session[:paths_visited].include?(@path_name)
  end
  
  # Delete and recreate the session with only the dashboard path in its array - again, my prefrence
  def delete_session_paths_visited_if_on_admin
    # if request.env['ORIGINAL_FULLPATH'] == '/admin'
    if request.original_fullpath == '/admin' 
      session.delete(:paths_visited)
      # Check if your version of ruby supports "if not" or use unless
      session[:paths_visited] = Array.new(1, admin_root_path) if not session[:paths_visited]
    end
  end

  def set_tags_nav
    @all_tags = Tag.all
  end

  def set_right_article_nav
    @articles_right_nav = Article.all
  end


  
  
end
