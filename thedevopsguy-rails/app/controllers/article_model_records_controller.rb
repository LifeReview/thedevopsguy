class ArticleModelRecordsController < InheritedResources::Base

  private

    def article_model_record_params
      params.require(:article_model_record).permit(:article_id, :model_id)
    end
end

