class VehiclesController < InheritedResources::Base

  def index
    @vehicles = Vehicle.all
  end

  def scrape
    url = 'https://devhumor.com/'
    response = VehiclesSpider.process(url)    
    if response[:status] == :completed && response[:error].nil?      
      flash.now[:notice] = "Successfully scraped url"
    else
      flash.now[:alert] = response[:error]
    end
  rescue StandardError => e
    flash.now[:alert] = "Error: #{e}"
  end

  private

    def vehicle_params
      params.require(:vehicle).permit(:title, :stock_type, :exterior_color, :interior_color, :transmission, :drivetrain, :price, :miles)
    end
end

