class IdolsController < InheritedResources::Base
  before_action :set_idol, only: [:show, :edit, :update, :destroy]
  # GET /idols
  # GET /idols.json
  def index
    # if params[:name]
    #   @idols = Idol.search_by_full_name(params[:name]).page(params[:page])
    # else
    #   @idols = Idol.all.page(params[:page])
    # end

    @search = params["search"]
    if @search.present?
      @name = @search["name"]
      @idols = Idol.where("name LIKE ?", "%#{@name}%").page(params[:page])
    else
      @idols = Idol.all.page(params[:page])
    end
    
  end

  # GET /idols/1
  # GET /idols/1.json
  def show
    set_meta_tags title: @idol.name,
      description: 'Show the page in single.',
      keywords: 'Site, single, Page, Show'
      # @video_tags = Tag.joins(:article_tag_records).where( "article_tag_records.article_id" => @article.id).uniq
      @idols_articles = @idol.articles
  end

  # GET /idols/new
  def new
    @idol = Idol.new
  end

  # GET /idols/1/edit
  def edit
  end

  # POST /idols
  # POST /idols.json
  def create
    @idol = Idol.new(idol_params)

    respond_to do |format|
      if @idol.save
        format.html { redirect_to @idol, notice: 'Idol was successfully created.' }
        format.json { render :show, status: :created, location: @idol }
      else
        format.html { render :new }
        format.json { render json: @idol.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /idols/1
  # PATCH/PUT /idols/1.json
  def update
    respond_to do |format|
      if @idol.update(idol_params)
        format.html { redirect_to @idol, notice: 'Idol was successfully updated.' }
        format.json { render :show, status: :ok, location: @idol }
      else
        format.html { render :edit }
        format.json { render json: @idol.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /idols/1
  # DELETE /idols/1.json
  def destroy
    @idol.destroy
    respond_to do |format|
      format.html { redirect_to idols_url, notice: 'Idol was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  private

    # Use callbacks to share common setup or constraints between actions.
    def set_idol
      @idol = Idol.find(params[:id])
    end

    def idol_params
      params.require(:idol).permit(:alias, :country, :city, :astrological, :birthday, :debutyear, :haircolor, :eyecolor, :height, :measurement, :tattoos, :piercings, :piercing)
    end
end

