json.extract! idol, :id, :alias, :country, :city, :astrological, :birthday, :debut-year, :hair-color, :eye-color, :height, :measurement, :tattoos, :piercings, :created_at, :updated_at
json.url idol_url(idol, format: :json)
