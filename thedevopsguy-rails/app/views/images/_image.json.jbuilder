json.extract! image, :id, :article_id, :name, :url, :height, :width, :format, :created_at, :updated_at
json.url image_url(image, format: :json)
