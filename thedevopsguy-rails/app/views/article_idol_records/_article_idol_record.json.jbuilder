json.extract! article_idol_record, :id, :article_id, :idol_id, :created_at, :updated_at
json.url article_idol_record_url(article_idol_record, format: :json)
