json.extract! video, :id, :article_id, :name, :url, :format, :length, :created_at, :updated_at
json.url video_url(video, format: :json)
