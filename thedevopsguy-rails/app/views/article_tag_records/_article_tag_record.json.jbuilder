json.extract! article_tag_record, :id, :article_id, :tag_id, :created_at, :updated_at
json.url article_tag_record_url(article_tag_record, format: :json)
