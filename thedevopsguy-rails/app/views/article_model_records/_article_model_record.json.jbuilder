json.extract! article_model_record, :id, :article_id, :model_id, :created_at, :updated_at
json.url article_model_record_url(article_model_record, format: :json)
