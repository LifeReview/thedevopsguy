json.extract! interaction, :id, :article_id, :user_id, :status, :created_at, :updated_at
json.url interaction_url(interaction, format: :json)
