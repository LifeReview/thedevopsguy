json.extract! album, :id, :name, :description, :type, :status, :created_at, :updated_at
json.url album_url(album, format: :json)
