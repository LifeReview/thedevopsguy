

$(document).ready(function()
{
    if ( $( "#article-info-scroll" ).length ) {
        $('html, body').animate({
            scrollTop: $("#article-info-scroll").offset().top
        }, 1000);
    }
    
    
    $.ajax({
        type: "GET",
        // url: "/articles/",
        dataType: "json",
        dataType:"json",
        data: {},
        success:function(result){            
            if (result.interaction_status == "like"){
                // alert(result.interaction_status);
                $(".btn-upvote").css({
                        'background' : '#4287f5',
                        'border' : '#4287f5',
                        'color' : 'white'
                    });
            }
            else if (result.interaction_status == "dislike"){
                $(".btn-downvote").css({
                    'background' : '#4287f5',
                    'border' : '#4287f5',
                    'color' : 'white'
                });           
            }
        }        
    }); 
});
