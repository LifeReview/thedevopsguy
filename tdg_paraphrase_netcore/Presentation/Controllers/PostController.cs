using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tdg_paraphrase_netcore.Models;
using tdg_paraphrase_netcore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace tdg_paraphrase_netcore.Controllers
{
    public class PostController : Controller
    {
        private ApplicationDbContext _context;
        private int PageSize = 4;
        public PostController(ApplicationDbContext context)
        {
            _context = context;
        }

        // [HttpGet]
        // public IActionResult Index()  
        // {  
        //     IEnumerable<Post> post = context.Set<Post>().ToList().Select(s => new Post  
        //     {  
        //         PostId= s.PostId,  
        //         Title = s.Title,                  
        //     });  
        //     return View("Index", post);  
        // }  

        // GET: Products
         // GET: Products/Create

        [HttpGet]       
        public async Task<IActionResult> Index()
        {
            return View(await _context.Posts.ToListAsync());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PostId,Completed,Description,ImageUrl,Link,Price,Title")] Post post)
        {
            if (ModelState.IsValid)
            {
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // [HttpPost]
        // [ActionName("Edit")]
        // [ValidateAntiForgeryToken]
        // public async Task<ActionResult> EditAsync([Bind("PostId,Completed,Description,ImageUrl,Link,Price,Title")] Post post)
        // {
        //     if (ModelState.IsValid)
        //     {
        //         await _context.Update<Post>(Post post);
        //         return RedirectToAction("Index");
        //     }

        //     return View(post);
        // }
    }
}