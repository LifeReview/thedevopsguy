namespace tdg_paraphrase_netcore.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using tdg_paraphrase_netcore.Models;

    public interface IPostCosmosDbService
    {
        Task<IEnumerable<Post>> GetPostsAsync(string query);
        Task<Post> GetPostAsync(string id);
        Task AddPostAsync(Post post);
        Task UpdatePostAsync(string id, Post post);
        Task DeletePostAsync(string id);
    }

}