using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace tdg_paraphrase_netcore.Models 
{
    public static class SeedData 
    {
        public static void EnsurePopulated(IApplicationBuilder app) 
        {
            ApplicationDbContext context = app.ApplicationServices
                .GetRequiredService<ApplicationDbContext>();
                if (!context.Posts.Any()) 
                {
                    context.Posts.AddRange(
                        new Post 
                        {
                            Title = "Kayak", 
                            Description = "A boat for one person",
                            Link = "https://abc.com", 
                            Price = 275 
                        }            
                    );
                    context.SaveChanges();
                }
        }
    }
} 