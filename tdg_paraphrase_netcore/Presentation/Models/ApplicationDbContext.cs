using Microsoft.EntityFrameworkCore;
using System.Linq;
// using System.Data.Entity.Core.EntityClient;

namespace tdg_paraphrase_netcore.Models
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions options)
            :base(options)
        {
        }
        public DbSet<Post> Posts { get; set; }        
    }
}