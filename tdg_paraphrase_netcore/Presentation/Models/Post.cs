
using tdg_paraphrase_netcore.CustomAttributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tdg_paraphrase_netcore.Models
{
    [Table("Post")]
    public class Post
    {
        readonly ApplicationDbContext _context;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PostId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public int Year { get; set; }
        public bool Completed { get; set; }        
        public decimal Price { get; set; }
        public string ImageUrl { get; set; }
        public bool IsApproved { get; set; }

        // public int CreatePost(CreatePostCommand cmd)
        // {

        // }
    }

}