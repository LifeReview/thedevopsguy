using System.Collections.Generic;
namespace tdg_paraphrase_netcore.Models {
    public class EFPostRepository : IPostRepository 
    {
        private ApplicationDbContext context;
        public EFPostRepository(ApplicationDbContext ctx) 
        {
            context = ctx;
        }
        public IEnumerable<Post> Posts => context.Posts;
    }
}