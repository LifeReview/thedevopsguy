using System.Collections.Generic;
namespace tdg_paraphrase_netcore.Models 
{ 
    public interface IPostRepository 
    {
        IEnumerable<Post> Posts { get; }
    }
}