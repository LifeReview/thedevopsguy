﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace tdg_paraphrase_netcore.Migrations
{
    public partial class IsApprovedToPost : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Post",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Post");
        }
    }
}
